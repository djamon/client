from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()

version = {}
with open(path.join(here, 'client', 'version.py'), encoding='utf-8') as f:
    exec(f.read(), version)

setup(
    name='djamon_client',
    version=version['__version__'],
    description='djamon client daemon',
    long_description=long_description,
    url='https://bitbucket.org/djamon/worker',
    author='Josh Lindsey',
    author_email='joshua.s.lindsey@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 2.7',
    ],
    keywords='monitoring devops',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=['protobuf', 'argh', 'pyyaml'],
    entry_points={
        'console_scripts': [
            'djamon_client=client.cli:run',
        ],
    },
)

