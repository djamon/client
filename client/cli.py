from argh import ArghParser
from argh.decorators import arg
import client.node
from client.status import Status


@arg('-d', '--daemonize', help="Fork to a background process")
def start(daemonize=False):
    node = client.node.load_or_create('./node')
    status = Status(node=node, name='test status')

    print(str(status.pb_status))


def run():
    parser = ArghParser()
    parser.add_commands([start])

    parser.dispatch()

