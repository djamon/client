from protocol import djamon_pb2
import uuid


class Status(object):
    def __init__(self, node, name):
        self.node = node
        self.pb_status = djamon_pb2.Status(uuid=str(uuid.uuid5(uuid.UUID(self.node.uuid), name)),
                                           name=name)
