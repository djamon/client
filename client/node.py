from protocol import djamon_pb2
from uuid import uuid1
from socket import gethostname
import os
import yaml


class UnableToWriteNodeFile(BaseException):
    pass


def load(file_path):
    if os.path.isfile(file_path) and os.access(file_path, os.R_OK):
        with open(file_path, 'r') as f:
            return yaml.load(f.read())
    else:
        return None


def create(file_path):
    dir = os.path.dirname(file_path)
    if os.path.exists(dir) and os.access(dir, os.W_OK):
        node = djamon_pb2.Node(uuid=str(uuid1()), name=gethostname())
        with open(file_path, 'w') as f:
            f.write(yaml.dump(node))
        return node
    else:
        raise UnableToWriteNodeFile("Unable to create node file")


def load_or_create(file_path):
    node = load(file_path)
    if node is None:
        node = create(file_path)

    return node

